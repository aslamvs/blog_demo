demo_blog
=========

"Blog" is an abbreviated version of "weblog," which is a term used to describe websites that maintain an ongoing chronicle of information. A blog features diary-type commentary and links to articles on other websites, usually presented as a list of entries in reverse chronological order. Blogs range from the personal to the political, and can focus on one narrow subject or a whole range of subjects.




Getting Up and Running Locally
---------------------


The steps below will get you up and running with a local development environment. We assume you have the following installed:-

	pip
	virtualenv
	Mysql


Then install the requirements for your local development:-

	pip install -r requirements/local.txt


Then, create a PostgreSQL database with the following command, where [project_slug] is what value you entered for your project’s project_slug:


You can now run the usual Django migrate and runserver commands:

$ python manage.py migrate
$ python manage.py runserver



Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.





Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^







